FROM mariadb:10.3

ARG GIT_LFS_OBJECTS_REPO=git@gitlab.com:tmaczukin-test-projects/git-lfs-objects.git
ARG SSH_KEY

ENV MYSQL_ROOT_PASSWORD=example
ENV MYSQL_DATABASE=example

# Package requirements
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    git \
    git-lfs \
    openssh-client && \
    git lfs install --skip-repo

# Configure Git to access the repositories
RUN mkdir -p /root/.ssh/ && \
    eval $(ssh-agent -s) && \
    echo "$SSH_KEY" > ~/.ssh/id_rsa && \
    chmod 400 /root/.ssh/id_rsa && \
    ssh-keyscan gitlab.com > /root/.ssh/known_hosts && \
    git config --global user.email "user@example.com"

# Get the theme & theme images
RUN git version && \
    git clone ${GIT_LFS_OBJECTS_REPO} /git-lfs-objects && \
    du -msc /git-lfs-objects/*

# Cleaning the image
RUN apt-get purge -y \
    openssh-client \
    git-lfs \
    git && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    # Deleting the ssh key
    rm -rf /root/.ssh/id_rsa

